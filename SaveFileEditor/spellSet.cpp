
#include "utils\fileStream.h"
#include "spellSet.h"
#include "blowfish\cryptKey.h"
#include "exceptHandler.h"
#include <iterator>
#include <regex>
#include <sstream>

std::string SpellSet::GetMagicFileName()
{
	std::vector<uint8_t> srcV({ 0x77, 0x6A, 0xC0, 0x2B, 0x2B, 0x90, 0x9B, 0x59, 0x79, 0x81, 0x26, 0x91, 0x15, 0x69, 0xA6, 0x48 });
	std::string destS;
	using namespace CryptKey;
	std::vector<uint8_t> destV;
	cryptMod decrypt(m_key, CryptMode::DECRYPT);
	if (decrypt.StreamCrypt(srcV, destV))
	{
		uint32_t length = destV[0] & 0xf;
		std::transform(&destV[1], &destV[length+1], std::insert_iterator<std::string>(destS, destS.begin()), [](uint8_t chk) { return chk; });
	}
	return destS;
}

bool SpellSet::CheckBeforeRead()
{
	std::string cFileName(std::move(GetMagicFileName()));

	if (cFileName.length())
	{
		ChangeFileName(m_parentPath + cFileName);
		return true;
	}
	return false;
}

void SpellSet::ReportReadComplete(std::vector<uint8_t> *fstream)
{
	using namespace CryptKey;
	cryptMod decrypt(m_key, CryptMode::DECRYPT);
	std::vector<uint8_t> destV;
	if (decrypt.StreamCrypt(*fstream, destV))
	{
		std::transform(destV.begin(), destV.end(), std::insert_iterator<std::string>(m_stream, m_stream.begin()), [](uint8_t chk) { return chk; });
		if (CheckHeader())
		{
			Serialize();
			m_fileReady = true;
		}
		else
			m_exPtr->Append(ExceptMsg::EXMSG_KEYWORD_HEAD_MISMATCH, std::string());
	}
	else
		m_exPtr->Append(ExceptMsg::EXMSG_CRYPT_ERROR, std::string(), ExceptFlag::EXFLAG_CRITICAL);
}

SpellSet::SpellSet(const std::shared_ptr<ExceptionHandler> &exPtr, const std::string &_parentPath, const std::string &_fileName)
	: m_fileName(_fileName), m_key(22), FileStream(_fileName), m_parentPath(_parentPath + '\\'), m_exPtr(exPtr)
{
	m_fileReady = false;
}

bool SpellSet::Ready() const
{
	return m_fileReady;
}

uint32_t SpellSet::ValidCount(const std::map<std::string, std::string> &serialKeyM, std::list<lElement> &_spellList, const std::string &keyword)
{
	decltype((m_magicM)) magic = m_magicM;
	decltype((m_exPtr)) exPtr = m_exPtr;
	uint32_t sCount = 0;

	std::for_each(serialKeyM.begin(), serialKeyM.end(), [&sCount, magic, &_spellList, &exPtr, keyword] (decltype(*serialKeyM.begin()) node)
	{
		decltype(magic.end()) scResult(magic.find(node.second.substr(1, node.second.length() - 2)));
		if (scResult != magic.end())
		{
			bool endtoken = scResult->first.back() == 35;
			sCount += ((((scResult->second >> 8) & 0xff) != 0) ? 256 : 1);
			//_spellList.emplace_back(scResult->first, ((scResult->second >> 0x10) & 0xff) != 0);
			_spellList.emplace_back(endtoken ? std::string(scResult->first.begin(), scResult->first.end()-1) : scResult->first, ((scResult->second >> 0x10) & 0xff) != 0);
		}
		else
		{
			if (node.first.find(keyword) != std::string::npos)
				exPtr->Append(ExceptMsg::EXMSG_KEYWORD_MISMATCH, std::string(node.second), ExceptFlag::EXFLAG_NOTHING);
		}
	});
	if (sCount & 0xff && (sCount >> 8))
		return 0;
	return (sCount >> 8) ? (sCount >> 8) : sCount & 0xff;
}

bool SpellSet::CheckHeader()
{
	const std::string header("PowerMagicList");

	return header.compare(m_stream.substr(0, header.length())) == 0;
}

void SpellSet::Serialize()
{
	std::regex rx(R"(([\d]*): ([#\w]*))");
	std::map<std::string, uint32_t> &mInjector = m_magicM;
	auto toNumber = [](std::string src)->uint32_t { uint32_t res = 0; std::istringstream ss(src); ss >> res; return res; };

	std::transform(std::sregex_iterator(m_stream.begin(), m_stream.end(), rx), std::sregex_iterator(), std::insert_iterator<std::map<std::string, uint32_t>>(mInjector, mInjector.begin()),
		[toNumber](std::smatch magic)->std::pair<std::string, uint32_t> { return std::make_pair(*(--magic.end()), toNumber(*(++magic.begin()))); });
	//ShowOutput();
}

void SpellSet::ShowOutput()
{
	std::for_each(m_magicM.begin(), m_magicM.end(), [](std::pair<std::string, uint32_t> node)
	{
		std::cout << "Key=" << node.first
			<< ", value=" << node.second
			<< std::endl;
	});
}