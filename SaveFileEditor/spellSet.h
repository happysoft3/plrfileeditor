
#ifndef SPELL_SET_H__
#define SPELL_SET_H__

#include <iostream>
#include <vector>
#include <map>
#include <list>
#include <tuple>
#include <memory>

class ExceptionHandler;

class SpellSet : public FileStream
{
private:
	const uint8_t m_key;
	std::string m_fileName;
	std::string m_stream;
	const std::string m_parentPath;
	std::map<std::string, uint32_t> m_magicM;
	std::shared_ptr<ExceptionHandler> m_exPtr;
	bool m_fileReady;
private:
	std::string GetMagicFileName();
	virtual bool CheckBeforeRead() override;
	virtual void ReportReadComplete(std::vector<uint8_t> *fstream) override;
public:
	explicit SpellSet(const std::shared_ptr<ExceptionHandler> &exPtr, const std::string &_parentPath = {}, const std::string &_fileName = "file.txt");
	virtual ~SpellSet() { }
	bool Ready() const;
	using lElement = std::tuple<std::string, uint8_t>;
	uint32_t ValidCount(const std::map<std::string, std::string> &serialKeyM, std::list<lElement> &_spellList, const std::string &keyword);
private:
	bool CheckHeader();
	void Serialize();
	void ShowOutput();
};

#endif