
#ifndef PACKER_H__
#define PACKER_H__


#include "packerTypes.h"
#include <string>
#include <map>
#include <tuple>
#include <memory>

class SaveFormat;
class Packer;
using ReadPropertyPointer = bool (Packer::*)(uint16_t);

class Packer : public BinaryParser		//onlySavePacker
{
private:
	std::shared_ptr<SaveFormat> m_target;
	std::string m_plrFileName;
	std::map<uint32_t, ReadPropertyPointer> m_methodM;
	std::list<std::tuple<uint8_t, uint32_t, uint32_t>> m_posRecord;
	bool m_enableDebug;
	//std::shared_ptr<ExceptionHandler> m_exPtr;
public:
	explicit Packer(std::shared_ptr<SaveFormat> &target, const std::string fileName = "plrFile");
	virtual ~Packer() { }
private:
	bool ReadType(uint16_t offset);
	bool ReadMetaFieldLength(uint16_t offset);
	bool ReadClassType(uint16_t offset);
	bool ReadSomeFlags(uint16_t offset);
	bool ReadSpellListOneOf();
	bool ReadSpellList(uint16_t offset);
	bool ReadSpellSlotIndex(uint16_t offset);
	bool ReadPaddingUnk(uint16_t offset);	//align again here
	bool ReadPaddingNext(uint16_t offset);
	bool ReadPlrFileName(uint16_t offset);
	bool ReadColorField(uint16_t offset);
	bool ReadUserName(uint16_t offset);
	bool ReadLastMapName(uint16_t offset);
private:
	virtual bool CheckBeforeRead() override;
	void Regist(PackerTypes::PackType order, ReadPropertyPointer functionPtr, uint16_t offset = 0);
	bool Running();
	bool Packing(std::tuple<uint8_t, uint32_t, uint32_t> &aItem);
public:
	bool Packing();
	bool Unpacking();
private:
	void WriteColorTable();
	void WriteUserName();
	void WriteSpellList();
};

#endif