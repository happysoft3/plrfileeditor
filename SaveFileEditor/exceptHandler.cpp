

#include "exceptHandler.h"
#include <algorithm>
#include <iostream>

void ExceptionHandler::NotifyNoError()
{ }

void ExceptionHandler::GetAnError(uint32_t errCount)
{ }

void ExceptionHandler::NotifyCriticalError(ExceptMsg msgId)
{
	Print("***Critical Error! id=", static_cast<int>(msgId), "***");
}

void ExceptionHandler::PrintAll()
{
	std::for_each(m_exceptList.begin(), m_exceptList.end(), [this](ExceptMsgForm msg)
	{
		NotifyErrorMessage(Get<ExMsgFieldName::EX_MSG_ID>(msg), Get<ExMsgFieldName::EX_MSG_DESC>(msg), Get<ExMsgFieldName::EX_MSG_FLAG>(msg));
	});
}

ExceptionHandler::ExceptionHandler()
	: m_criticalError(nullptr), m_warningError(nullptr)
{
	//
}

void ExceptionHandler::NotifyErrorMessage(ExceptMsg msgId, const std::string &desc, ExceptFlag flag)
{
	Print("error code=", static_cast<uint32_t>(msgId), "description=", desc);
}

void ExceptionHandler::Append(ExceptMsgForm &exMsg)
{
	m_exceptList.emplace_back(exMsg);
}

void ExceptionHandler::Append(ExceptMsg msgId, std::string &desc, ExceptFlag flag)
{
	m_exceptList.emplace(m_exceptList.end(), msgId, desc, flag);
	if (flag == ExceptFlag::EXFLAG_WARNING)
		m_warningError = &(*m_exceptList.rbegin());
	if (flag == ExceptFlag::EXFLAG_CRITICAL)
	{
		m_criticalError = &(*m_exceptList.rbegin());
		NotifyCriticalError(std::get<EX_MSG_ID>(*m_criticalError));
	}
}

bool ExceptionHandler::HasWarning()
{
	return m_warningError != nullptr;
}

bool ExceptionHandler::HasCritical()
{
	return m_criticalError != nullptr;
}

bool ExceptionHandler::HasError()
{
	return HasWarning() || HasCritical();
}

void ExceptionHandler::Checking()
{
	if (!HasWarning() && !HasCritical())
		NotifyNoError();
	else
		GetAnError(m_exceptList.size());
	PrintAll();
}