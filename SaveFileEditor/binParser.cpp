
#include "utils\fileStream.h"
#include "binParser.h"
#include <algorithm>
#include "blowfish\cryptKey.h"
#include <iterator>

void BinaryParser::ReportReadComplete(std::vector<uint8_t> *fstream)
{
	CryptKey::cryptMod decrypt(m_cryptKey, CryptKey::CryptMode::DECRYPT);
	decrypt.StreamCrypt(*fstream, m_streamV);
}

void BinaryParser::ReportWriteReady(std::vector<uint8_t> *fstream)
{
	CryptKey::cryptMod encrypt(m_cryptKey, CryptKey::CryptMode::ENCRYPT);
	encrypt.StreamCrypt(m_outstreamV, *fstream);
}

BinaryParser::BinaryParser(const std::string &binFileName)
	: m_cryptKey(27), FileStream(binFileName), m_streamRd(0), m_streamRef(0)
{
	//
}

bool BinaryParser::StreamSeekLength(uint8_t length, std::vector<uint8_t> &destV, uint32_t uIndex)
{
	if (m_streamRd + uIndex + length < m_streamV.size())
	{
		std::vector<uint8_t>::iterator currentPos = m_streamV.begin() + m_streamRd + uIndex;
		destV.resize(static_cast<uint32_t>(length));
		std::transform(currentPos, currentPos + length, destV.begin(), [](uint8_t chunk) { return chunk; });
		m_streamRd += (length + uIndex);
		return true;
	}
	return false;
}

bool BinaryParser::StreamSeekPadding(uint16_t offset)
{
	uint32_t virtPos = m_streamRd + offset;
	uint32_t paddingPos = virtPos + ((8 - (virtPos % 8)) & 7);

	if (paddingPos < m_streamV.size())
	{
		m_streamRd = paddingPos;
		return true;
	}
	return false;
}

uint32_t BinaryParser::StreamTellg() const
{
	return m_streamRd;
}

bool BinaryParser::StreamWrite(uint32_t uIndex)
{
	if (uIndex >= m_streamRef)
	{
		std::transform(&m_streamV[m_streamRef], &m_streamV[uIndex],
			std::insert_iterator<std::vector<uint8_t>>(m_outstreamV, m_outstreamV.end()), [](uint8_t chk) { return chk; });
		m_streamRef = uIndex;
		return true;
	}
	return false;
}

void BinaryParser::StreamWrite(const std::vector<uint8_t> &srcV)
{
	std::transform(srcV.begin(), srcV.end(), std::insert_iterator<std::vector<uint8_t>>(m_outstreamV, m_outstreamV.end()), [](uint8_t cpy) { return cpy; });
}

bool BinaryParser::StreamWrite(void)
{
	if (m_streamRef < m_streamRd)
	{
		std::transform(m_streamV.begin() + m_streamRef, m_streamV.end(), 
			std::insert_iterator<std::vector<uint8_t>>(m_outstreamV, m_outstreamV.end()), [](uint8_t chk) { return chk; });
		return true;
	}
	return false;
}

bool BinaryParser::StreamWritePass(uint32_t length)
{
	if (m_streamRef + length < m_streamV.size())
	{
		m_streamRef += length;
		return true;
	}
	return false;
}

void BinaryParser::StreamWritePadding(uint32_t offset)
{
	uint32_t streamPos = m_outstreamV.size() + offset;
	if (streamPos % 8)
	{
		uint8_t padCount = (8 - (streamPos % 8));
		if (padCount)
		{
			std::vector<uint8_t> padV(padCount, 0);
			std::insert_iterator<std::vector<uint8_t>> vInjector(m_outstreamV, m_outstreamV.end());
			std::transform(padV.begin(), padV.end(), std::insert_iterator<std::vector<uint8_t>>(m_outstreamV, m_outstreamV.end()), [](uint8_t chk) { return chk; });
		}
	}
}

bool BinaryParser::OutputTesting()
{
	FileStream file("outputtest.bin", m_outstreamV, FileMode::FILE_MODE_WRITE);

	return file.FileProcess() > 0;
}