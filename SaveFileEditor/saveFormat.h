
#ifndef SAVE_FORMAT_H__
#define SAVE_FORMAT_H__

#include <iostream>
#include <list>
#include <string>
#include <vector>
#include <tuple>

namespace FormatMethod
{
	constexpr int listFieldBuffer = 2;
	constexpr int slotFieldLength = 6;
}

enum class GameType
{
	TYPE_NONE,
	TYPE_QUEST,
	TYPE_ARENA
};

enum class CSpellCount		//unReferenced
{
	WARRIOR_COUNT = 25,
	WIZARD_COUNT = 34,
	CONJURER_COUNT = 34,
	INVALID_COUNT = 0
};

enum class CharType
{
	CLASS_WARRIOR,
	CLASS_WIZARD,
	CLASS_CONJURER,
	CLASS_INVALID
};

enum class ColorTarget
{
	COLOR_HAIR,
	COLOR_SKIN,
	COLOR_MUST,
	COLOR_BEARD,
	COLOR_GOATEE
};

class SaveFormat
{
public:
	using spellElement = std::tuple<std::string, uint8_t, uint32_t>;
private:
	GameType m_gType;
	uint32_t m_unknownFront;
	uint16_t m_someFlag;
	CharType m_classType;
	std::list<spellElement> m_spellList;
	std::string m_plrFullName;
	std::string m_lastMapName;
	std::wstring m_userName;
	std::vector<uint8_t> m_colorTable;
	std::string m_parentPath;

	std::uint32_t m_spellOrder;

public:
	explicit SaveFormat();
	virtual ~SaveFormat() { }

	//@property
	GameType GetGameType() const;
	void SetGameType(uint8_t _type);
	uint32_t GetUnknownFront() const;
	void SetUnknownFront(uint32_t _unknown);
	CharType GetClassType() const;
	void SetClassType(uint8_t _type);
	uint16_t GetSomeFlags() const;
	void SetSomeFlags(uint16_t flags);
	void SpellListShow();
	void SpellListExport(std::list<spellElement> **listPtr);
	void SpellListPush(std::string spellName, uint8_t castType, uint32_t someValue);
	void SetPlrFileFullName(const std::string &src);
	std::string GetPlrFileFullName() const;
	void CopyColorTable(std::vector<uint8_t> &movSrc);
	void CopyColorTable(std::vector<uint8_t> &&movcpy);
	std::string GetColorField(ColorTarget cTarget);
	std::vector<uint8_t> ColorTable() const;
	void SetUserName(const std::vector<uint16_t> &nameV);
	void SetUserName(const std::wstring &username);
	std::string GetUserName();
	std::wstring GetUserNameW() const;
	void SetLastMapName(std::string &src);
	void SetParentPath(const std::string &src);
	std::string GetParentPath() const;
	uint32_t MetaFieldLength();

	void ShowInfo();
private:
	std::string ShowUserName();
};

#endif


