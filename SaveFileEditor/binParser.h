
#ifndef BINARY_PARSER_H__
#define BINARY_PARSER_H__


#include <iostream>
#include <string>
#include <vector>

class FileStream;

class BinaryParser : public FileStream
{
private:
	const uint8_t m_cryptKey;
	std::vector<uint8_t> m_streamV;
	uint32_t m_streamRd;

	uint32_t m_streamRef;
	std::vector<uint8_t> m_outstreamV;	//added
private:
	virtual void ReportReadComplete(std::vector<uint8_t> *fstream) override;
	virtual void ReportWriteReady(std::vector<uint8_t> *fstream) override;
public:
	explicit BinaryParser(const std::string &binFileName);
	template <class T>
	bool StreamSeek(T &dest, uint32_t uIndex = 0);
	bool StreamSeekLength(uint8_t length, std::vector<uint8_t> &destV, uint32_t uIndex = 0);
	bool StreamSeekPadding(uint16_t offset = 0);
	uint32_t StreamTellg() const;

	bool StreamWrite(uint32_t uIndex);
	void StreamWrite(const std::vector<uint8_t> &srcV);
	bool StreamWrite(void);	//RemainByteWrite
	bool StreamWritePass(uint32_t length);
	void StreamWritePadding(uint32_t offset = 0);
	template <class T>
	void StreamWriteAny(T any);
	bool OutputTesting();
};

template <class T>
bool BinaryParser::StreamSeek(T &dest, uint32_t uIndex)
{
	if (m_streamRd + uIndex + sizeof(T) < m_streamV.size())
	{
		dest = *reinterpret_cast<T *>(&m_streamV[uIndex + m_streamRd]);
		m_streamRd += (uIndex + sizeof(T));
		return true;
	}
	return false;
}

template <class T>
void BinaryParser::StreamWriteAny(T any)
{
	for (int i = sizeof(T); i; --i)
	{
		m_outstreamV.push_back(any & 0xff);
		any = any >> 8;
	}
}

#endif