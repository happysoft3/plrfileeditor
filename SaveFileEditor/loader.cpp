
#include "utils\fileStream.h"
#include "saveFormat.h"
#include "exceptHandler.h"
#include "loader.h"
#include "binParser.h"
#include "packer.h"
#include "modifier.h"
#include "spellSet.h"
#include "utils\stringUtils.h"



void FormatLoader::NotifyNoError()
{
	std::cout << ((m_inputType == InputType::INPUT_PLR) ? "UNPACKING" : "PACKING") << std::endl;
	m_target->ShowInfo();
	std::cout << m_sourceFile << std::endl;
}

void FormatLoader::GetAnError(uint32_t errCount)
{
	std::cout << "get an error: " << errCount << std::endl;
}

void FormatLoader::Packing()
{
	Modifier mod(m_sourceFile, m_exSelfPt, m_target);

	do
	{
		mod.FileProcess();
		if (HasError()) break;
		std::string plrFile;
		if (mod.GetFileURL(plrFile))
		{
			Packer packer(m_target, plrFile);
			if (packer.FileProcess())
			{
				if (!packer.Unpacking())
				{
					Append(ExceptMsg::EXMSG_UNPACKING_ERROR, std::string());
					break;
				}
				if (!mod.SendToTarget()) break;
				if (packer.Packing())
				{
					packer.ChangeFileMode(FileMode::FILE_MODE_WRITE);
					if (!packer.FileProcess())
						Append(ExceptMsg::EXMSG_FAIL_PACKER_OUTPUT, std::string());
				}
				else
					Append(ExceptMsg::EXMSG_PACKING_ERROR, std::string());
			}
		}
	} while (false);
}

void FormatLoader::UnPacking()
{
	Packer unpacker(m_target, m_sourceFile);

	do
	{
		if (!unpacker.FileProcess()) break;
		if (!unpacker.Unpacking())
			Append(ExceptMsg::EXMSG_UNPACKING_ERROR, std::string());
		if (HasError()) break;

		Modifier mod("OUTPUT", m_exSelfPt, m_target, PlugMode::PLUG_ON);
		if (!mod.FileProcess())
			Append(ExceptMsg::EXMSG_FAIL_MAKEFILE, std::string());
	} while (false);
}

FormatLoader::FormatLoader(char **env)
	: m_sourceFile(env[1]), m_inputType(InputType::INPUT_ANY), m_exSelfPt(this)
{
	m_target = std::make_shared<SaveFormat>();
	m_target->SetParentPath(env[0]);
}

void FormatLoader::Load()
{
	do
	{
		if (!StringUtils::GetFileExtension(m_sourceFile).compare(".plr"))
		{
			m_inputType = InputType::INPUT_PLR;
			UnPacking();
		}
		else if (!StringUtils::GetFileExtension(m_sourceFile).compare(".txt"))
		{
			m_inputType = InputType::INPUT_TXT;
			Packing();
		}
		else break;
		Checking();
	} while (false);
}