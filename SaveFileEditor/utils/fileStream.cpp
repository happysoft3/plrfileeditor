


#include <iostream>
#include <iterator>
#include <algorithm>
#include "fileStream.h"


void FileStream::ReportReadComplete(std::vector<uint8_t> *stream)
{ }

void FileStream::ReportWriteReady(std::vector<uint8_t> *stream)
{ }

bool FileStream::CheckBeforeRead()
{
	return true;
}

uint32_t FileStream::Read()
{
	uint32_t fSize = 0;
	std::ifstream fin(m_curFileName, std::ios::in | std::ios::binary);
	fin >> std::noskipws;
	if (fin.is_open())
	{
		fin.seekg(0, std::ios::end);
		m_fstream.clear();
		fSize = static_cast<uint32_t>(fin.tellg());
		m_fstream.resize(fSize);
		fin.seekg(0, std::ios::beg);
		std::transform(std::istream_iterator<uint8_t>(fin), std::istream_iterator<uint8_t>(), m_fstream.begin(), [](uint8_t chk) { return chk; });
		fin.close();
		ReportReadComplete(&m_fstream);
	}
	return fSize;
}

uint32_t FileStream::Write()
{
	uint32_t fSize = 0;
	std::ofstream fout(m_curFileName, std::ios::binary);

	if (fout.is_open())
	{
		std::vector<uint8_t> *wrStream = &m_fstream;
		ReportWriteReady(wrStream);

		std::for_each(wrStream->begin(), wrStream->end(), [&fout](uint8_t wrChunk) { fout << wrChunk; });
		fout.close();
		fSize = wrStream->size();
	}
	return fSize;
}

FileStream::FileStream(const std::string &_fileName, FileMode _mode)
	: m_curFileName(_fileName), m_mode(_mode)
{
	//
}

FileStream::FileStream(const std::string &_fileName, std::vector<uint8_t> &_cpyStream, FileMode _mode)
	: m_curFileName(_fileName), m_mode(_mode)
{
	m_fstream = _cpyStream;
}

FileStream::FileStream(const FileStream &cpy)
{
	m_curFileName = cpy.m_curFileName;
	m_fstream = cpy.m_fstream;
	m_mode = cpy.m_mode;
}

FileStream::~FileStream()
{
	//
}

uint32_t FileStream::FileProcess()
{
	switch (m_mode)
	{
	case FileMode::FILE_MODE_READ:
		return CheckBeforeRead() ? Read() : 0;
	case FileMode::FILE_MODE_WRITE:
		return Write();
	case FileMode::FILE_MODE_OVERWRITE:
		break;
	}
	return 0;
}

uint32_t FileStream::GetStreamLength() const
{
	return m_fstream.size();
}

std::vector<uint8_t> &FileStream::GetFileStream()
{
	return m_fstream;
}

bool FileStream::ChangeFileMode(FileMode _mode)
{
	bool pResult = m_mode != _mode;
	if (pResult)
	{
		m_mode = _mode;
	}
	return pResult;
}

void FileStream::ChangeFileName(const std::string &_fn)
{
	m_curFileName = std::move(_fn);
}

void FileStream::operator<<(std::vector<uint8_t> &srcStream)
{
	m_fstream.clear();
	m_fstream.resize(srcStream.size());
	std::transform(srcStream.begin(), srcStream.end(), m_fstream.begin(), [](uint8_t uchk)->uint8_t { return uchk; });
}

void FileStream::operator<<(const std::string &srcStrStream)
{
	m_fstream.clear();
	m_fstream.resize(srcStrStream.length());
	std::transform(srcStrStream.begin(), srcStrStream.end(), m_fstream.begin(), [](char chk)->uint8_t { return static_cast<uint8_t>(chk); });
}

std::string FileStream::CurrentFileName() const
{
	return m_curFileName;
}