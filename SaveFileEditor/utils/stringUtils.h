
#ifndef STRING_UTILS_H__
#define STRING_UTILS_H__

#include <iostream>

namespace StringUtils
{
	bool UnicodeToAnsi(std::wstring &src, std::string &dest);
	bool UnicodeToUtf8(std::wstring &src, std::string &dest);
	bool Utf8ToUnicode(std::string &src, std::wstring &dest);
	std::string GetFileExtension(const std::string &fullName);
}

#endif
