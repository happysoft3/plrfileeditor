
#ifndef FILE_STREAM_H__
#define FILE_STREAM_H__

#include <fstream>
#include <string>
#include <vector>

enum class FileMode
{
	FILE_MODE_READ,
	FILE_MODE_WRITE,
	FILE_MODE_OVERWRITE
};

class FileStream
{
private:
	std::vector<uint8_t> m_fstream;
	std::string m_curFileName;
	FileMode m_mode;

private:
	virtual void ReportReadComplete(std::vector<uint8_t> *fstream);		//파일 읽기 완료보고
	virtual void ReportWriteReady(std::vector<uint8_t> *fstream);	//파일 쓰기 준비완료
	virtual bool CheckBeforeRead();	//파일 읽기 전 확인
	uint32_t Read();			//파일 읽기(스트림에 저장):	읽은 파일의 스트림 크기 반환
	uint32_t Write();			//파일 쓰기(스트림으로 부터 저장): 스트림 내용을 파일로 쓰기
public:
	explicit FileStream(const std::string &_fileName, FileMode _mode = FileMode::FILE_MODE_READ);	//파일명, 파일처리 타입(읽기, 쓰기)
	FileStream(const std::string &_fileName, std::vector<uint8_t> &_cpyStream, FileMode _mode = FileMode::FILE_MODE_READ);
	explicit FileStream(const FileStream &cpy);		//복사생성자: 스트림 간 깊은복사 수행
	virtual ~FileStream();		//소멸자
	virtual uint32_t FileProcess();			//파일 처리(모드에 따라 읽기 또는 쓰기로 분기됨 (오버라이딩 대비)
	uint32_t GetStreamLength() const;	//파일 스트림 크기 반환
	std::vector<uint8_t> &GetFileStream();
	bool ChangeFileMode(FileMode _mode);
	void ChangeFileName(const std::string &_fn);
	void operator<<(std::vector<uint8_t> &srcStream);
	void operator<<(const std::string &srcStrStream);
protected:
	std::string CurrentFileName() const;
};

#endif