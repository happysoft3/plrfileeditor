
#ifndef MODIFIER_H__
#define MODIFIER_H__


#include <map>
#include <iostream>
#include <string>
#include <memory>


class SaveFormat;
class FileStream;
class ExceptionHandler;

enum class PlugMode
{
	PLUG_OFF,
	PLUG_ON,
	PLUG_BIND
};

class SpellSet;

class Modifier : public FileStream
{
private:
	std::shared_ptr<SaveFormat> m_target;
	PlugMode m_plugMode;
	const std::string m_fileExp;
	const std::uint32_t m_maxNameLength;
	std::map<std::string, std::string> m_keyStore;
	std::string m_modFileName;
	std::string m_modStream;
	std::map<std::string, std::string> m_serialKeyMap;
	bool m_state;
	std::unique_ptr<SpellSet> m_spellChecker;
	std::list<std::tuple<std::string, uint8_t>> m_spellList;

	std::shared_ptr<ExceptionHandler> m_exPtr;
	std::string m_spellKeyword;
private:
	std::string WriteFileName();
	virtual void ReportReadComplete(std::vector<uint8_t> *fstream);
	virtual void ReportWriteReady(std::vector<uint8_t> *fstream);
public:
	explicit Modifier(const std::string modFileName, const std::shared_ptr<ExceptionHandler> &exPtr,
		const std::shared_ptr<SaveFormat> &_target, PlugMode _plugMode = PlugMode::PLUG_OFF);
	virtual ~Modifier() { }
private:
	void SetSpellKeyword();
	void SpellKeyJoin();
	bool KeyValidScan();
	void KeyJoin();
	bool Serialize();
	void ModifyColorTable();
	void ModifyUserName();
	void ModifySpellList();
public:
	bool SendToTarget();
	bool GetFileURL(std::string &dest);
};

#endif