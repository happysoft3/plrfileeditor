
#include "utils\fileStream.h"
#include "binParser.h"
#include "saveFormat.h"
#include "packer.h"
#include "utils\stringUtils.h"

#include <algorithm>
#include <iterator>


Packer::Packer(std::shared_ptr<SaveFormat> &_target, const std::string fileName)
	: BinaryParser(fileName), m_plrFileName(fileName), m_target(_target)
{
	m_enableDebug = false;
}

bool Packer::ReadType(uint16_t offset)
{
	uint8_t type = 0;
	bool pResult = StreamSeek(type);
	m_target->SetGameType(type);
	
	return m_target->GetGameType() == GameType::TYPE_ARENA;
}

bool Packer::ReadMetaFieldLength(uint16_t offset)
{
	uint32_t unk = 0;
	bool pResult = StreamSeek(unk, offset);

	m_target->SetUnknownFront(unk);
	return pResult;
}

bool Packer::ReadClassType(uint16_t offset)
{
	uint8_t clType = 0;
	bool pResult = StreamSeek(clType);

	m_target->SetClassType(clType);
	return pResult;
}

bool Packer::ReadSomeFlags(uint16_t offset)
{
	uint16_t flags = 0;
	bool pResult = StreamSeek(flags, offset);

	m_target->SetSomeFlags(flags);
	return pResult;
}

bool Packer::ReadSpellListOneOf()
{
	uint8_t chunkLength = 0;
	while (StreamSeek(chunkLength))
	{
		if (!chunkLength) break;
		std::vector<uint8_t> destV;
		if (StreamSeekLength(chunkLength, destV))
		{
			uint8_t selfTarget = 0;
			bool pResult = StreamSeek(selfTarget);	//target: 0, self: 1
			m_target->SpellListPush(std::string(destV.begin(), destV.end()), selfTarget, 0);
			return pResult;
		}
	}
	return false;
}

bool Packer::ReadSpellList(uint16_t offset)
{
	for (int i = static_cast<int>((m_target->GetClassType() == CharType::CLASS_WARRIOR) ? CSpellCount::WARRIOR_COUNT : CSpellCount::WIZARD_COUNT); i; --i)
	{
		if (!ReadSpellListOneOf())
			return false;
	}
	return true;
}

bool Packer::ReadSpellSlotIndex(uint16_t offset)
{
	uint8_t spellSlotIdx;
	uint8_t trapSlotIdx;
	uint32_t unk4100;

	StreamSeek(spellSlotIdx);
	StreamSeek(trapSlotIdx);
	return StreamSeek(unk4100);
}

bool Packer::ReadPaddingUnk(uint16_t offset)
{
	return StreamSeekPadding(offset);
}

bool Packer::ReadPaddingNext(uint16_t offset)
{
	uint32_t unk;

	return StreamSeek(unk, offset);
}

bool Packer::ReadPlrFileName(uint16_t offset)
{
	uint16_t length;

	if (StreamSeek(length, offset))
	{
		std::vector<uint8_t> destV;
		if (StreamSeekLength(static_cast<uint8_t>(length), destV))
		{
			m_target->SetPlrFileFullName(std::string(destV.begin(), destV.end()));
			return true;
		}
	}
	return false;
}

bool Packer::ReadColorField(uint16_t offset)
{
	//머리, 피부, 콧, 턱, 구렛
	std::vector<uint8_t> destV;
	if (StreamSeekLength(15, destV, offset))
	{
		m_target->CopyColorTable(destV);
		return true;
	}
	return false;
}

bool Packer::ReadUserName(uint16_t offset)
{
	uint8_t length;

	if (StreamSeek(length, offset))
	{
		std::vector<uint8_t> readV;
		if (StreamSeekLength(length * 2, readV))
		{
			std::uint8_t order = 0;
			std::uint8_t tmpStore;
			std::vector<uint16_t> destV;
			std::for_each(readV.begin(), readV.end(), [&order, &tmpStore, &destV](uint8_t chk)
			{
				if (order & 1) destV.push_back(tmpStore | (chk << 8));
				else tmpStore = chk;
				++order;
			});
			m_target->SetUserName(destV);
			return true;
		}
	}
	return false;
}

bool Packer::ReadLastMapName(uint16_t offset)
{
	uint8_t length;

	if (StreamSeek(length, offset))
	{
		std::vector<uint8_t> destV;
		bool pResult = StreamSeekLength(length, destV);
		m_target->SetLastMapName(std::string(destV.begin(), destV.end()));
		return pResult;
	}
	return false;
}

bool Packer::CheckBeforeRead()
{
	return StringUtils::GetFileExtension(m_plrFileName).compare(".plr") == 0;
}

void Packer::Regist(PackerTypes::PackType order, ReadPropertyPointer functionPtr, uint16_t offset)
{
	auto createKey = [](uint8_t _order, uint16_t _offset)->uint32_t { return (_order << 0x18) | _offset; };
	m_methodM.emplace(createKey(static_cast<uint8_t>(order), offset), functionPtr);
}

bool Packer::Running()
{
	std::map<std::uint32_t, ReadPropertyPointer>::iterator cpos = m_methodM.begin();
	auto getOrderNumberLd = [](uint32_t _key)->uint8_t { return _key >> 0x18; };
	auto getVOffsetLd = [](uint32_t _key)->uint16_t { return _key & 0xffff; };

	while (cpos != m_methodM.end())
	{
		m_posRecord.emplace_back(getOrderNumberLd(cpos->first), StreamTellg() + getVOffsetLd(cpos->first), 0);
		if (!(this->*(*cpos).second)(getVOffsetLd(cpos->first)))
			return false;
		std::get<2>(*m_posRecord.rbegin()) = StreamTellg();
		++cpos;
	}
	return true;
}

bool Packer::Packing(std::tuple<uint8_t, uint32_t, uint32_t> &aItem)
{
	//Order call
	uint32_t length = std::get<2>(aItem) - std::get<1>(aItem);

	if (length >> 0x10)
		return false;
	StreamWrite(std::get<1>(aItem));
	switch (static_cast<PackerTypes::PackType>(std::get<0>(aItem)))
	{
	case PackerTypes::PackType::META_FIELD_LENGTH:
		StreamWriteAny(m_target->MetaFieldLength());
		StreamWritePass(length);
		break;
	case PackerTypes::PackType::SPELL_LIST:	//이 필드 다음 패딩을 신경써야한다..
		WriteSpellList();
		StreamWritePass(length);
		break;
	case PackerTypes::PackType::COLOR_FIELD:
		WriteColorTable();
		StreamWritePass(length);
		break;
	case PackerTypes::PackType::USERNAME:
		WriteUserName();
		StreamWritePass(length);
		break;
	case PackerTypes::PackType::PADDING1:
	case PackerTypes::PackType::PADDING2:
		StreamWritePadding();
		StreamWritePass(length);
		break;
	default:
		break;
	}
	if (m_enableDebug)
	{
		std::cout << "packer.cpp::Packer::Packing::order="
			<< static_cast<uint32_t>(std::get<0>(aItem))
			<< ", offset= " << std::get<1>(aItem)
			<< ", length= " << length << std::endl;
	}
	return true;
}

bool Packer::Packing()
{
	bool pResult = true;
	std::for_each(m_posRecord.begin(), m_posRecord.end(), [this, &pResult](std::tuple<uint8_t, uint32_t, uint32_t> node)
	{
		if (!Packing(node))
			pResult = false;
	});
	if (pResult)
	{
		pResult = StreamWrite(); //남은 바이트 쓰기
		if (m_enableDebug) OutputTesting();
	}
	return pResult;
}

bool Packer::Unpacking()
{
	using namespace PackerTypes;

	Regist(PackType::TYPE, &Packer::ReadType);
	Regist(PackType::META_FIELD_LENGTH, &Packer::ReadMetaFieldLength, 7);
	Regist(PackType::SOME_FLAG, &Packer::ReadSomeFlags, 4);
	Regist(PackType::CCLASS, &Packer::ReadClassType);
	Regist(PackType::SPELL_LIST, &Packer::ReadSpellList);
	Regist(PackType::SPELL_SLOT, &Packer::ReadSpellSlotIndex);
	Regist(PackType::PADDING1, &Packer::ReadPaddingUnk, 2); //added 2
	Regist(PackType::PADDING_NEXT, &Packer::ReadPaddingNext, 8);
	Regist(PackType::FILENAME, &Packer::ReadPlrFileName, 2);
	Regist(PackType::COLOR_FIELD, &Packer::ReadColorField, 0x11); //0x10);
	Regist(PackType::USERNAME, &Packer::ReadUserName, 5); //6);
	Regist(PackType::MAPNAME, &Packer::ReadLastMapName, 3);
	Regist(PackType::UNKNOWN2, &Packer::ReadPaddingNext, 1);
	Regist(PackType::PADDING2, &Packer::ReadPaddingUnk);
	Regist(PackType::PADDING_NEXT2, &Packer::ReadPaddingNext);

	return Running();
}

void Packer::WriteColorTable()
{
	StreamWrite(m_target->ColorTable());
}

void Packer::WriteUserName()
{
	std::wstring usrName(m_target->GetUserNameW());
	std::vector<uint8_t> destV;
	std::insert_iterator<std::vector<uint8_t>> vInjector(destV, destV.end());

	vInjector = usrName.length() & 0xf;
	std::for_each(usrName.begin(), usrName.end(), [&vInjector](uint16_t tPiece)
	{
		vInjector = tPiece & 0xff;
		vInjector = tPiece >> 8;
	});
	StreamWrite(destV);
}

void Packer::WriteSpellList()
{
	std::list<SaveFormat::spellElement> *listGet = nullptr;

	m_target->SpellListExport(&listGet);
	std::for_each(listGet->begin(), listGet->end(), [this, listGet](decltype(*(listGet->begin())) node)
	{
		std::string itemName(std::get<0>(node));
		std::vector<uint8_t> wrV(itemName.length() + 2);
		std::transform(itemName.begin(), itemName.end(), wrV.begin() + 1, [](uint8_t mov) { return mov; });
		*wrV.begin() = itemName.length() & 0x3f;
		*wrV.rbegin() = std::get<1>(node);
		StreamWrite(wrV);
	});
}