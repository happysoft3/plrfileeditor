
#ifndef LOADER_H__
#define LOADER_H__

#include <string>
#include <memory>


class SaveFormat;
class ExceptionHandler;

enum class InputType
{
	INPUT_PLR,
	INPUT_TXT,
	INPUT_ANY
};

class FormatLoader : public ExceptionHandler
{
private:
	std::string m_sourceFile;
	InputType m_inputType;
	std::shared_ptr<SaveFormat> m_target;
	std::shared_ptr<ExceptionHandler> m_exSelfPt;
private:
	virtual void NotifyNoError() override;
	virtual void GetAnError(uint32_t errCount) override;
	void Packing();
	void UnPacking();
public:
	explicit FormatLoader(char **env);
	void Load();
};

#endif