
#include "saveFormat.h"
#include "utils\stringUtils.h"
#include <algorithm>
#include <utility>

SaveFormat::SaveFormat()
	:m_gType(GameType::TYPE_NONE), m_classType(CharType::CLASS_INVALID)
{
	m_someFlag = 0;
	m_unknownFront = 0;
	m_spellOrder = 5;
}

GameType SaveFormat::GetGameType() const
{
	return m_gType;
}

void SaveFormat::SetGameType(uint8_t _type)
{
	switch (_type)
	{
	case 2:
		m_gType = GameType::TYPE_QUEST;
		break;
	case 7:
		m_gType = GameType::TYPE_ARENA;
		break;
	default:
		m_gType = GameType::TYPE_NONE;
	}
}

uint32_t SaveFormat::GetUnknownFront() const
{
	return m_unknownFront;
}

void SaveFormat::SetUnknownFront(uint32_t _unknown)
{
	m_unknownFront = _unknown;
}

CharType SaveFormat::GetClassType() const
{
	return m_classType;
}

void SaveFormat::SetClassType(uint8_t _type)
{
	switch (_type)
	{
	case 0:
		m_classType = CharType::CLASS_WARRIOR;
		break;
	case 1:
		m_classType = CharType::CLASS_WIZARD;
		break;
	case 2:
		m_classType = CharType::CLASS_CONJURER;
		break;
	default:
		m_classType = CharType::CLASS_INVALID;
	}
}

uint16_t SaveFormat::GetSomeFlags() const
{
	return m_someFlag;
}

void SaveFormat::SetSomeFlags(uint16_t flags)
{
	m_someFlag = flags;
}

void SaveFormat::SpellListShow()
{
	std::for_each(m_spellList.begin(), m_spellList.end(), [](std::tuple<std::string, uint8_t, uint32_t> node)
	{
		std::cout << "SpellName: " << std::get<0>(node)
			<< ", targetType: " << (std::get<1>(node) ? "self" : "target")
			<< std::endl;
	});
}

void SaveFormat::SpellListExport(std::list<SaveFormat::spellElement> **listPtr)
{
	*listPtr = &m_spellList;
}

void SaveFormat::SpellListPush(std::string spellName, uint8_t castType, uint32_t someValue)
{
	if (!(m_spellOrder % 5))
		m_spellOrder += 5;
	m_spellList.emplace_back(spellName, castType, ++m_spellOrder);
}

void SaveFormat::SetPlrFileFullName(const std::string &src)
{
	m_plrFullName = src;
}

std::string SaveFormat::GetPlrFileFullName() const
{
	return m_plrFullName;
}

void SaveFormat::CopyColorTable(std::vector<uint8_t> &movSrc)
{
	m_colorTable = std::move(movSrc);
}

void SaveFormat::CopyColorTable(std::vector<uint8_t> &&movcpy)
{
	m_colorTable = std::forward<std::vector<uint8_t>>(movcpy);	//here
}

std::string SaveFormat::GetColorField(ColorTarget cTarget)
{
	if (!m_colorTable.size()) return{};
	std::string dest;
	uint8_t index = static_cast<uint8_t>(cTarget) * 3;

	std::vector<uint8_t>::iterator tableIter = m_colorTable.begin() + index;
	std::for_each(tableIter, tableIter + 3, [&dest](uint8_t col)
	{ dest += (std::to_string(static_cast<uint32_t>(col & 0xff)) + ", "); });
	return dest.substr(0, dest.size() - 2);
}

std::vector<uint8_t> SaveFormat::ColorTable() const
{
	return m_colorTable;
}

void SaveFormat::SetUserName(const std::vector<uint16_t> &nameV)
{
	m_userName = std::move(std::wstring(nameV.begin(), nameV.end()));
}

void SaveFormat::SetUserName(const std::wstring &username)
{
	m_userName = username;
}

std::string SaveFormat::GetUserName()
{
	using namespace StringUtils;
	std::string dest;

	UnicodeToUtf8(m_userName, dest);
	return dest;
}

std::wstring SaveFormat::GetUserNameW() const
{
	return m_userName;
}

void SaveFormat::SetLastMapName(std::string &src)
{
	m_lastMapName = std::move(src);
}

void SaveFormat::SetParentPath(const std::string &src)
{
	size_t separate = src.find_last_of("\\");

	m_parentPath = src.substr(0, separate);
}

std::string SaveFormat::GetParentPath() const
{
	return m_parentPath;
}

uint32_t SaveFormat::MetaFieldLength()
{
	uint32_t listFieldLength = 0;

	std::for_each(m_spellList.begin(), m_spellList.end(), [&listFieldLength](spellElement item)
	{
		listFieldLength += (std::get<0>(item).length() + FormatMethod::listFieldBuffer);
	});
	return listFieldLength + FormatMethod::slotFieldLength;
}

void SaveFormat::ShowInfo()
{
	std::cout << "plrFileFullName: " << m_plrFullName << std::endl;
	std::cout << "lastMap: " << m_lastMapName << std::endl;
	std::cout << "userName: " << ShowUserName() << std::endl;
	std::cout << "spellList: " << m_spellList.size() << std::endl;
	std::cout << "HairColor: " << GetColorField(ColorTarget::COLOR_HAIR) << std::endl;
	std::cout << "SkinColor: " << GetColorField(ColorTarget::COLOR_GOATEE) << std::endl;
	SpellListShow();
}

std::string SaveFormat::ShowUserName()
{
	std::string dest;

	StringUtils::UnicodeToAnsi(m_userName, dest);
	return dest;
}