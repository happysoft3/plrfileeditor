
#include "exceptHandler.h"
#include "loader.h"
#include <memory>

int main(int argc, char **argv)
{
	if (argc == 2)
	{
		ExceptionHandler *loader(new FormatLoader(argv));
		dynamic_cast<FormatLoader*>(loader)->Load();

		::getchar();
	}
	return 0;
}