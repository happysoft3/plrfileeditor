
#include "saveFormat.h"
#include "utils\fileStream.h"
#include "modifier.h"
#include "spellSet.h"
#include "utils\stringUtils.h"
#include "exceptHandler.h"
#include <algorithm>
#include <regex>
#include <iterator>
#include <sstream>


std::string Modifier::WriteFileName()
{
	std::string fileName(m_target->GetParentPath());
	std::string plrFullName(m_target->GetPlrFileFullName());
	size_t tokenFirst = plrFullName.find_last_of('\\');
	size_t tokenLast = plrFullName.find_last_of('.');

	return fileName + plrFullName.substr(tokenFirst, tokenLast - tokenFirst) + m_fileExp;
}

void Modifier::ReportReadComplete(std::vector<uint8_t> *fstream)
{
	std::transform(fstream->begin(), fstream->end(), std::insert_iterator<std::string>(m_modStream, m_modStream.begin()), [](uint8_t chunk) { return chunk; });
	if (!Serialize())
		m_exPtr->Append(ExceptMsg::EXMSG_FAIL_SERIALIZE, std::string("serialize error"), ExceptFlag::EXFLAG_CRITICAL);
}

void Modifier::ReportWriteReady(std::vector<uint8_t> *fstream)
{
	auto strWrapperLd = [](std::string &_src)->std::string { return std::string('"' + _src + '"'); };
	std::map<std::string, std::string>::iterator sIter = m_keyStore.find("[PlrFormatHeader]");
	if (sIter != m_keyStore.end())
	{
		std::string output((*sIter).second);
		m_keyStore.erase(sIter);
		try
		{
			std::for_each(m_keyStore.begin(), m_keyStore.end(), [&output, strWrapperLd](std::pair<std::string, std::string> node)
			{ output += ('\n' + node.first + " = " + strWrapperLd(node.second)); });	//Need '"' Wrapping!!
		}
		catch (const std::out_of_range &oor)
		{
			m_exPtr->Append(ExceptMsg::EXMSG_OUTOF_RANGE, std::string(oor.what()), ExceptFlag::EXFLAG_WARNING);
			return;
		}
		(*fstream).clear();
		(*fstream).resize(output.size());
		std::transform(output.begin(), output.end(), fstream->begin(), [](uint8_t chk)->char { return chk; });
	}
}

Modifier::Modifier(const std::string modFileName, const std::shared_ptr<ExceptionHandler> &exPtr, const std::shared_ptr<SaveFormat> &_target, PlugMode _plugMode)
	: m_fileExp(".txt"), FileStream(modFileName), m_target(_target), m_plugMode(_plugMode), m_maxNameLength(18), m_spellChecker(nullptr), m_exPtr(exPtr)
{
	if (m_plugMode == PlugMode::PLUG_ON)
	{
		ChangeFileMode(FileMode::FILE_MODE_WRITE);
		ChangeFileName(WriteFileName());
	}
	else
	{
		m_spellChecker = std::make_unique<SpellSet>(m_exPtr, m_target->GetParentPath());
		m_spellChecker->FileProcess();
	}
	KeyJoin();
}

void Modifier::SetSpellKeyword()
{
	m_spellKeyword = std::move(std::string(((m_target->GetClassType() != CharType::CLASS_WARRIOR) ? "SpellSet" : "AbilitySet")));
}

void Modifier::SpellKeyJoin()
{
	std::string &keyword = m_spellKeyword;
	std::list<SaveFormat::spellElement> *listPt = nullptr;
	decltype(m_keyStore) &keyStore = m_keyStore;
	
	m_target->SpellListExport(&listPt);
	std::for_each(listPt->begin(), listPt->end(), [keyword, &keyStore](decltype(*listPt->begin()) node)
	{
		keyStore.emplace(keyword + std::to_string(std::get<2>(node)), std::get<0>(node));
	});
}

bool Modifier::KeyValidScan()
{
	std::map<std::string, std::string> *serial = &m_serialKeyMap;
	std::pair<uint8_t, uint8_t> chksum(std::make_pair(static_cast<uint8_t>(m_keyStore.size()), 0));

	std::for_each(m_keyStore.begin(), m_keyStore.end(), [&serial, &chksum](decltype(*m_keyStore.begin()) node)
	{ chksum.second += (serial->find(node.first) != serial->end()) ? 1 : 0; });
	return chksum.first == chksum.second;
}

void Modifier::KeyJoin()
{
	bool isPlug = m_plugMode == PlugMode::PLUG_ON;
	if (isPlug)
	{
		m_keyStore.emplace("[PlrFormatHeader]", "[PlrFormatHeader]");
		SetSpellKeyword();
	}

	m_keyStore.emplace(std::move(std::string("Nickname")), 
		std::move(isPlug ? std::string(m_target->GetUserName()) : std::string()));
	m_keyStore.emplace(std::move(std::string("FileURL")), 
		std::move(isPlug ? std::string(m_target->GetPlrFileFullName()) : std::string()));
	m_keyStore.emplace(std::move(std::string("HairColor")), 
		std::move(isPlug ? std::string(m_target->GetColorField(ColorTarget::COLOR_HAIR)) : std::string()));
	m_keyStore.emplace(std::move(std::string("SkinColor")), 
		std::move(isPlug ? std::string(m_target->GetColorField(ColorTarget::COLOR_SKIN)) : std::string()));
	m_keyStore.emplace(std::move(std::string("Mustache")), 
		std::move(isPlug ? std::string(m_target->GetColorField(ColorTarget::COLOR_MUST)) : std::string()));
	m_keyStore.emplace(std::move(std::string("BeardColor")), 
		std::move(isPlug ? std::string(m_target->GetColorField(ColorTarget::COLOR_BEARD)) : std::string()));
	m_keyStore.emplace(std::move(std::string("GoateeColor")), 
		std::move(isPlug ? std::string(m_target->GetColorField(ColorTarget::COLOR_GOATEE)) : std::string()));
	if (isPlug)
		SpellKeyJoin();
}

bool Modifier::Serialize()
{
	if (!m_modStream.length()) return false;
	if (m_modStream.find("[PlrFormatHeader]") == std::string::npos) return false;

	std::regex expr(R"(([\w]*)[\t=\s\n]*("[^"]*"))");
	std::map<std::string, std::string> serialKeymap;
	std::for_each(std::sregex_iterator(m_modStream.begin(), m_modStream.end(), expr), std::sregex_iterator(), [&serialKeymap](std::smatch mList)
	{ serialKeymap.emplace(std::move(*(++mList.begin())), std::move(*(--mList.end()))); });
	m_serialKeyMap = std::move(serialKeymap);
	return KeyValidScan();
}

void Modifier::ModifyColorTable()
{
	//lambda, regex checking
	std::vector<uint8_t> destV;
	std::regex expr(R"(\"([0-9]{1,3})[\t,\s]*([0-9]{1,3})[\t,\s]*([0-9]{1,3})\")");
	auto colorZerofillLd = [](std::vector<uint8_t> &_destV) { _destV.resize(_destV.size() + 3); };
	auto colorParserLd = [&destV, expr](const std::string &keyword)->bool
	{
		std::smatch _mlist;
		if (std::regex_search(keyword, _mlist, expr))
		{
			std::for_each(++_mlist.begin(), _mlist.end(), [&destV](std::string _col)
			{
				uint32_t _colrbyte;
				std::istringstream(_col) >> _colrbyte;
				destV.push_back(_colrbyte & 0xff);
			});
			return true;
		}
		return false;
	};
	if (!colorParserLd(m_serialKeyMap.at("HairColor"))) colorZerofillLd(destV);
	if (!colorParserLd(m_serialKeyMap.at("SkinColor"))) colorZerofillLd(destV);
	if (!colorParserLd(m_serialKeyMap.at("Mustache"))) colorZerofillLd(destV);
	if (!colorParserLd(m_serialKeyMap.at("BeardColor"))) colorZerofillLd(destV);
	if (!colorParserLd(m_serialKeyMap.at("GoateeColor"))) colorZerofillLd(destV);
	m_target->CopyColorTable(std::move(destV));
}

void Modifier::ModifyUserName()
{
	using namespace StringUtils;

	std::string src(m_serialKeyMap.at("Nickname"));
	std::wstring wdest;
	if (src.length() > m_maxNameLength)
		src.resize(m_maxNameLength);
	Utf8ToUnicode(src.substr(1, src.length() - 2), wdest);
	m_target->SetUserName(wdest);
}

void Modifier::ModifySpellList()
{
	std::list<SaveFormat::spellElement> *listPt = nullptr;
	decltype((m_spellList)) slist = m_spellList;

	m_target->SpellListExport(&listPt);
	std::transform(m_spellList.begin(), m_spellList.end(), listPt->begin(), [](decltype(*m_spellList.end()) sitem)->SaveFormat::spellElement
	{
		return std::make_tuple(std::get<0>(sitem), std::get<1>(sitem), 0);	//spell target 을 결정할 수 있어야함...
	});
}

bool Modifier::SendToTarget()
{
	SetSpellKeyword();
	if (m_spellChecker->Ready())
	{
		uint32_t chkResult = m_spellChecker->ValidCount(m_serialKeyMap, m_spellList, m_spellKeyword);
		CharType clType = m_target->GetClassType();

		auto DoubleChecking = [chkResult, clType]()->bool
		{
			switch (clType)
			{
			case CharType::CLASS_WARRIOR:
				if (chkResult ^ static_cast<uint32_t>(CSpellCount::WARRIOR_COUNT))
					return false;
				break;
			case CharType::CLASS_WIZARD:
			case CharType::CLASS_CONJURER:
				if (chkResult ^ static_cast<uint32_t>(CSpellCount::WIZARD_COUNT))
					return false;
				break;
			}
			return true;
		};
		if (DoubleChecking())
		{
			ModifyColorTable();
			ModifyUserName();
			ModifySpellList();
			return true;
		}
	}
	m_exPtr->Append(ExceptMsg::EXMSG_SEND_FAIL, std::string());
	return false;
}

bool Modifier::GetFileURL(std::string &dest)
{
	std::map<std::string, std::string>::iterator urlFind = m_serialKeyMap.find("FileURL");

	if (urlFind != m_serialKeyMap.end())
	{
		dest = urlFind->second.substr(1, urlFind->second.length() - 2);
		return true;
	}
	return false;
}