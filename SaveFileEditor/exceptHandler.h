
#ifndef EXCEPT_HANDLER_H__
#define EXCEPT_HANDLER_H__

#include <list>
#include <string>
#include <tuple>

enum class ExceptFlag
{
	EXFLAG_NOTHING = 0,
	EXFLAG_WARNING = 1,
	EXFLAG_CRITICAL = 2
};

enum class ExceptMsg
{
	EXMSG_MISSING_LISTFILE,
	EXMSG_KEYWORD_MISMATCH,
	EXMSG_CRYPT_ERROR,
	EXMSG_KEYWORD_HEAD_MISMATCH,
	EXMSG_OUTOF_RANGE,
	EXMSG_SEND_FAIL,
	EXMSG_FAIL_SERIALIZE,
	EXMSG_FAIL_PACKER_OUTPUT,
	EXMSG_PACKING_ERROR,
	EXMSG_UNPACKING_ERROR,
	EXMSG_FAIL_MAKEFILE
};

class ExceptionHandler
{
public:
	enum ExMsgFieldName
	{
		EX_MSG_ID,
		EX_MSG_DESC,
		EX_MSG_FLAG
	};
	template <ExMsgFieldName name, class... Types>
	typename std::tuple_element<static_cast<size_t>(name), std::tuple<Types...>>::type& Get(std::tuple<Types...> &t)
	{
		return std::get<static_cast<std::size_t>(name), Types...>(t);
	}
	using ExceptMsgForm = std::tuple<ExceptMsg, std::string, ExceptFlag>;
private:
	std::list<ExceptMsgForm> m_exceptList;
	ExceptMsgForm *m_criticalError;
	ExceptMsgForm *m_warningError;
private:
	virtual void NotifyNoError();
	virtual void GetAnError(uint32_t errCount);
	virtual void NotifyCriticalError(ExceptMsg msgId);
	void PrintAll();
	template <class T>
	void Print(T arg) { std::cout << arg << std::endl; }
	template <class T, class... Types>
	void Print(T arg, Types... args)
	{
		std::cout << arg << static_cast<char>(0x20);
		Print(args...);
	}
public:
	virtual void NotifyErrorMessage(ExceptMsg msgId, const std::string &desc, ExceptFlag flag);
	explicit ExceptionHandler();
	virtual ~ExceptionHandler() { }
	void Append(ExceptMsgForm &exMsg);
	void Append(ExceptMsg msgId, std::string &desc, ExceptFlag flag = ExceptFlag::EXFLAG_WARNING);
	bool HasWarning();
	bool HasCritical();
	bool HasError();
	void Checking();
};

#endif